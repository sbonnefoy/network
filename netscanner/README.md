This is a basic network scanner. 
You can ping an host, find open ports, and try to 
retrieve the banner. 

You need to install scapy for the ping function to work.