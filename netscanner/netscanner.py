import socket
from socket import gethostbyaddr
from scapy.all import IP, ICMP, sr1


def ping(target):
   ip = IP(dst=target)
   icmp = ICMP()
   response = sr1(ip/icmp, verbose=0)
   if response:
       print("Host %s responds to ping" %target)

def is_port_open(ip, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection_status = sock.connect_ex((ip, port))
    if connection_status == 0:
       return True

def scan_tcp_port(ip, port):
  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  sock.settimeout(3)
  try:
        #print('socket connection')
        sock.connect((ip,port))
        #print('socket sending')
        sock.send('hello world'.encode())
        #print('retrieve banner')
        banner = sock.recv(4096)
        try:
            print(banner.decode())
        except:
            print(banner)
        return port, banner
  except:
        pass


def scan_udp_port(ip, port):
    #send to udp
    sock_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock_udp.sendto('hello world'.encode(), (ip, port))
    banner = sock_udp.recvfrom(4096)
    try:
        print(banner.decode())
    except:
        print(banner)



if __name__ == "__main__":
   scan_tcp = []
   scan_udp = []
   open_ports = []
   target = '192.168.1.76'

   #Ping target machine
   ping(target)

    #check open ports on target machine
   for port in range(0,3307):
       if is_port_open(target, port):
           open_ports.append(port)

   print("Found open ports:")
   print(open_ports)

   for port in open_ports:
       print('\nscanning tcp port %i' %port)
       scan_tcp.append(scan_tcp_port(target, port))


